cd $HOME

if [ -d "2022-vision-code" ]
then
    rm -rf 2022-vision-code
fi

git clone https://gitlab.com/growingstems/frc-836-the-robobees/2022-vision-code.git
cd 2022-vision-code

sed -i "s/pi/$USERNAME/g" cargotracker.service
sudo cp cargotracker.service /lib/systemd/system/
sudo chmod 644 /lib/systemd/system/cargotracker.service

mkdir build
cd build
cmake ..
make

sudo systemctl daemon-reload
sudo systemctl enable cargotracker.service

