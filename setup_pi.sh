sudo apt update
sudo apt upgrade -y
sudo apt install -y vim git cmake tmux
sudo apt install -y build-essential pkg-config python3-dev python3-numpy libjpeg-dev libpng-dev libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev libv4l-dev v4l-utils

echo "interface eth0" | sudo tee -a /etc/dhcpcd.conf
echo "static ip_address=10.8.36.12/24" | sudo tee -a /etc/dhcpcd.conf
echo "static routers=10.8.36.1" | sudo tee -a /etc/dhcpcd.conf

git config --global user.name "Driver Station"
git config --global user.email "frc_driver_station@robobees.org"
git config --global pull.rebase false

git submodule update --init --recursive 2> /dev/null
if [ ! -d "opencv" ]
then
    git clone https://gitlab.com/codydg/get-opencv-working.git
    cd get-opencv-working
    git submodule update --init --recursive
fi

if [ -d "build" ]
then
    rm -rf build
fi
mkdir build
cd build

cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D BUILD_opencv_python3=OFF \
    -D BUILD_opencv_java=OFF \
    -D BUILD_opencv_gapi=OFF \
    -D BUILD_opencv_objc=OFF \
    -D BUILD_opencv_js=OFF \
    -D BUILD_opencv_ts=OFF \
    -D BUILD_opencv_dnn=OFF \
    -D BUILD_opencv_calib3d=OFF \
    -D BUILD_opencv_objdetect=OFF \
    -D BUILD_opencv_stitching=OFF \
    -D BUILD_opencv_ml=OFF \
    -D BUILD_opencv_world=OFF \
    -D BUILD_EXAMPLES=OFF \
    ../opencv && make && sudo make install && echo "No errors detected." || echo "ERRORS DETECTED!"

